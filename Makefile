SVG_FILES = $(shell find . -type f -name '*.svg' ! -path "./TheSimpsons/*" ! -path "./public/*" ! -path "./includes/*")
CLEAN=*~ *.rtf *.ps *.log *.dvi *.aux *.out *.html *.bak *.toc *.pl *.4ct *.4tc *.lg *.sxw *.tmp *.xref *.idv *.tns fiches/okr.pdf

%.svg.pdf: %.svg
	inkscape --export-type="pdf" --export-filename="$@" $<

%.svg.png: %.svg
	inkscape --export-type="png" --export-filename="$@" $<

fiches/okr.pdf: fiches/okr.svg.pdf fiches/choix-okr.svg.pdf
	pdftk fiches/okr.svg.pdf fiches/choix-okr.svg.pdf cat output fiches/okr.pdf

svg2pdf: $(patsubst %.svg,%.svg.pdf,$(SVG_FILES))

svg2png: $(patsubst %.svg,%.svg.png,$(SVG_FILES))

all: svg2png svg2pdf fiches/okr.pdf

ci:
	inotify-hookable -f Makefile $(patsubst %, -f %, $(SVG_FILES)) -c "make all"

clean:
	rm -rf $(CLEAN) public

